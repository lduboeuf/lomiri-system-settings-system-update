# Czech translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2021-02-03 19:22+0000\n"
"Last-Translator: Milan Korecký <milan.korecky@gmail.com>\n"
"Language-Team: Czech <https://translate.ubports.com/projects/ubports/"
"system-settings/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Nikdy"

#: ../plugins/system-update/ImageUpdatePrompt.qml:48
msgid "Cancel"
msgstr "Zrušit"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr "Neznámé"

#: ../plugins/system-update/InstallationFailed.qml:28
msgid "OK"
msgstr "Přijmout"

#: ../plugins/system-update/ChangelogExpander.qml:42
#, qt-format
msgid "Version %1"
msgstr "Verze %1"

#: ../plugins/system-update/ChannelSettings.qml:32
msgid "Channel settings"
msgstr "Nastavení kanálu"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Stable"
msgstr "Stabilní"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr "Kandidát na vydání"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr "Vývojový"

#: ../plugins/system-update/ChannelSettings.qml:99
msgid "Fetching channels"
msgstr "Načítání kanálů"

#: ../plugins/system-update/ChannelSettings.qml:108
msgid "Channel to get updates from:"
msgstr "Kanál pro získávání aktualizací:"

#: ../plugins/system-update/ChannelSettings.qml:124
msgid "Switching channel"
msgstr "Přepínání kanálu"

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Automatické stahování"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Stahovat budoucí aktualizace automaticky:"

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Přes Wi-Fi"

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Přes kterékoliv datové spojení"

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Přenos dat může být zpoplatněn."

#: ../plugins/system-update/DownloadHandler.qml:174
#: ../plugins/system-update/InstallationFailed.qml:25
msgid "Installation failed"
msgstr "Instalace se nezdařila"

#: ../plugins/system-update/FirmwareUpdate.qml:36
#: ../plugins/system-update/FirmwareUpdate.qml:157
msgid "Firmware Update"
msgstr "Aktualizace firmware"

#: ../plugins/system-update/FirmwareUpdate.qml:133
msgid "There is a firmware update available!"
msgstr "Je k dispozici aktualizace firmware!"

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "Firmware is up to date!"
msgstr "Firmware je aktuální!"

#: ../plugins/system-update/FirmwareUpdate.qml:169
msgid "The device will restart automatically after installing is done."
msgstr "Po dokončení instalace bude zařízení automaticky restartováno."

#: ../plugins/system-update/FirmwareUpdate.qml:185
msgid "Install and restart now"
msgstr "Nainstalovat a restartovat nyní"

#: ../plugins/system-update/FirmwareUpdate.qml:228
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""
"Stahují se a instalují aktualizace firmware – může to trvat několik minut…"

#: ../plugins/system-update/FirmwareUpdate.qml:235
msgid "Checking for firmware update"
msgstr "Zjišťování dostupnosti aktualizace firmware"

#: ../plugins/system-update/GlobalUpdateControls.qml:84
msgid "Checking for updates…"
msgstr "Zjišťování dostupnosti aktualizací…"

#: ../plugins/system-update/GlobalUpdateControls.qml:89
msgid "Stop"
msgstr "Zastavit"

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:116
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "%1 dostupná aktualizace"
msgstr[1] "%1 dostupné aktualizace"
msgstr[2] "%1 dostupných aktualizací"

#: ../plugins/system-update/GlobalUpdateControls.qml:126
msgid "Update all…"
msgstr "Aktualizovat vše…"

#: ../plugins/system-update/GlobalUpdateControls.qml:128
msgid "Update all"
msgstr "Aktualizovat vše"

#: ../plugins/system-update/ImageUpdatePrompt.qml:30
msgid "Update System"
msgstr "Aktualizovat systém"

#: ../plugins/system-update/ImageUpdatePrompt.qml:32
msgid "The device needs to restart to install the system update."
msgstr "Pro instalaci systémových aktualizací musí být přístroj restartován."

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "Connect the device to power before installing the system update."
msgstr "Před instalací systémové aktualizace připojte zařízení k nabíječce."

#: ../plugins/system-update/ImageUpdatePrompt.qml:38
msgid "Restart & Install"
msgstr "Restartovat a nainstalovat"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:37 ../build/po/settings.js:348
msgid "Updates"
msgstr "Aktualizace"

#: ../plugins/system-update/PageComponent.qml:163
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr "Připojit k Internetu a zkontrolovat aktualizace."

#: ../plugins/system-update/PageComponent.qml:165
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr "Software je aktuální"

#: ../plugins/system-update/PageComponent.qml:168
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr ""
"Server aktualizací neodpovídá.\n"
"Zkuste to později."

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/ReinstallAllApps.qml:164
msgid "Updates Available"
msgstr "Dostupné aktualizace"

#: ../plugins/system-update/PageComponent.qml:311
msgid "Recent updates"
msgstr "Nedávné aktualizace"

#: ../plugins/system-update/PageComponent.qml:374
#: ../plugins/system-update/UpdateSettings.qml:32
msgid "Update settings"
msgstr "Nastavení aktualizací"

#: ../plugins/system-update/ReinstallAllApps.qml:36
#: ../plugins/system-update/ReinstallAllApps.qml:95
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr "Přeinstalovat všechny aplikace"

#: ../plugins/system-update/ReinstallAllApps.qml:85
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""
"Použijte toto pro získání všech nejnovějších aplikací, které jsou obvykle "
"potřebné po přechodu na novější hlavní verzi systému, "

#: ../plugins/system-update/UpdateDelegate.qml:119
msgid "Retry"
msgstr "Zkusit znovu"

#: ../plugins/system-update/UpdateDelegate.qml:124
msgid "Update"
msgstr "Aktualizovat"

#: ../plugins/system-update/UpdateDelegate.qml:126
msgid "Download"
msgstr "Stáhnout"

#: ../plugins/system-update/UpdateDelegate.qml:132
msgid "Resume"
msgstr "Pokračovat"

#: ../plugins/system-update/UpdateDelegate.qml:139
msgid "Pause"
msgstr "Pozastavit"

#: ../plugins/system-update/UpdateDelegate.qml:143
msgid "Install…"
msgstr "Nainstalovat…"

#: ../plugins/system-update/UpdateDelegate.qml:145
msgid "Install"
msgstr "Nainstalujte"

#: ../plugins/system-update/UpdateDelegate.qml:149
msgid "Open"
msgstr "Otevřít"

#: ../plugins/system-update/UpdateDelegate.qml:228
msgid "Installing"
msgstr "Instalace"

#: ../plugins/system-update/UpdateDelegate.qml:232
msgid "Paused"
msgstr "Pozastaveno"

#: ../plugins/system-update/UpdateDelegate.qml:235
msgid "Waiting to download"
msgstr "Čeká na stažení"

#: ../plugins/system-update/UpdateDelegate.qml:238
msgid "Downloading"
msgstr "Stahování"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:274
#, qt-format
msgid "%1 of %2"
msgstr "%1 z %2"

#: ../plugins/system-update/UpdateDelegate.qml:278
msgid "Downloaded"
msgstr "Staženo"

#: ../plugins/system-update/UpdateDelegate.qml:281
msgid "Installed"
msgstr "Nainstalováno"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:286
#, qt-format
msgid "Updated %1"
msgstr "Aktualizováno %1"

#: ../plugins/system-update/UpdateDelegate.qml:310
msgid "Update failed"
msgstr "Aktualizace se nezdařila"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Přes Wi-Fi"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "Vždy"

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr "Kanály"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:26
msgid "software"
msgstr "software"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:108
msgid "automatic"
msgstr "automaticky"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:210
msgid "click"
msgstr "klik"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:220
msgid "apps"
msgstr "aplikace"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:350
msgid "system"
msgstr "systém"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:354
msgid "update"
msgstr "aktualizace"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:358
msgid "application"
msgstr "aplikace"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:362
msgid "download"
msgstr "stáhnout"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:364
msgid "upgrade"
msgstr "přechod na novější verzi"

