include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
)

set(MOCK_DOWNLOADMANAGER_SRCS
    plugin.cpp
    MockMetadata.cpp
    MockSingleDownload.cpp
    MockDownloadManager.cpp
)

add_library(MockLomiriDownloadManager MODULE ${MOCK_DOWNLOADMANAGER_SRCS})

target_link_libraries(MockLomiriDownloadManager Qt5::Qml Qt5::Quick Qt5::Core)

add_lss_mock(Lomiri.DownloadManager 1.2 Lomiri/DownloadManager
             TARGETS MockLomiriDownloadManager)
